<?php

use App\Http\Controllers\AdminBookController;
use App\Http\Controllers\AdminCategorieController;
use App\Http\Controllers\LoginBookController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/loginbook', [LoginBookController::class, 'authenticate']);


Route::get('/vh', [AdminBookController::class, 'viewhome'])->name('vh');
Route::get('/recomended', [AdminBookController::class, 'recomended'])->name('recomended');

Route::resource('adminbooks', AdminBookController::class);
Route::resource('admincategories', AdminCategorieController::class);
Route::resource('users', UserController::class);

Route::get('/', function () {
    return Inertia\Inertia::render('Books/Index');
});

Route::get('/userget', function () {
    return Auth::check()? Auth::user(): null;
})->name('userget');

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return Inertia\Inertia::render('Books/Index');
})->name('dashboard');




/*
 PARA RUTAS DE UNA VEZ QUE BUSCAN LA ENTIDAD POR EL id PASADO COMO ARGUMENTO
use App\Http\Controllers\UserController;
use App\Models\User;

Route::get('users/{user}', [UserController::class, 'show']); //para buscar por columna diferente al id 'users/{user:column}'

public function show(User $user)
{
    return view('user.profile', ['user' => $user]);
}
 */

/* VALORES POR DEFAULT EN CAMPOS
    protected $attributes = [
        'delayed' => false,
    ];

*CARGAS ANSIOSAS cargar data de claves foraneas sin la necesidad de llamar al metodo de relacion
 protected $with = ['author'];  //array de models que tienen relaciones

*VALORES PERMITIDOS PARA CREACION EN MASA
    protected $fillable = ['name','issn'];

*PERMITE TODOS LOS ATRIBUTOS CREARLOS EN MASA
    protected $guarded = [];
*/


/*
 *METHODS SCOOPS FOR BUILDER QUERYS HELPERS
    public function scopeActive($query)
    {
        return $query->where('active', 1);
    }
    //Use or this way: $books = AdminBook::active()->otroscoop(este recibe params si quiero)->orderBy('created_at')->get();
    //Use orWhere() para unir dos scoops a modo de or sql $books = AdminBooks::popular()->orWhere->active()->get();
//php artisan make:controller PhotoController --resource --model=Photo
 */
