<?php

namespace App\Http\Controllers;

use App\Models\AdminBook;
use Illuminate\Http\Request;
use Inertia\Inertia;

class GuiaController extends Controller
{
    public function list(Request $request)
    {

        $books = AdminBook::all();
        return Inertia::render('Books/Index', [
            'books' => $books
        ]);






        /* UPDATE Y RECUPERAR
            $books = AdminBook::where('number', 'FR 900')->first();
            $flight->number = 'FR 456';
            $flight->refresh();
            $flight->number; // "FR 900"
         *ARMAR QUERYS
             $books = AdminBook::where('active', 1)
                   ->orderBy('name', 'desc')
                   ->take(10)
                   ->get();
        *BUSCAR UN REGISTRO
            $book = AdminBook::firstWhere('active', 1);
        *REGISTROS POR ARRAYS DE ID'S
            $books = AdminBook::find([1, 2, 3]);
        *EJECUTAR UNA FUNCIÓN SI NO ENCUENTRO UN REGISTRO
           $book = AdminBook::where('id', '>', 100)->firstOr(function () {
                // ...
            });
        *CALCULOS EN CONSULTA VALOR ESCALAR
            $count = AdminBook::where('id', 1)->count();
            $max = AdminBook::where('active', 1)->max('price');
         */
    }

    public function create(Request $request)
    {
        /*
        *CREATE FOR ASSOCIATIVE ARRAY
            $user = AdminBook::create([
                'first_name' => 'Taylor',
                'last_name' => 'Otwell',
                'title' => 'Developer',
            ]);
        *STORE IN BD for create
            $book = new AdminBook;
            $book->name = $request->name;
            $book->save();
        */
    }

    public function edit(Request $request)
    {
        /*
        *STORE IN BD for update
            $book = AdminBook::find(1);
            $book->name = 'wherever';
            $book->save();
        *MASSIVE UPDATE
           AdminBook::where('active', 1)
            ->where('destination', 'San Diego')
            ->update(['delayed' => 1]);
        */
    }

    public function store(Request $request)
    {


    }

    public function show(Request $request)
    {
        //
    }

    public function destroy(Request $request)
    {
        /*
        *ELIMINAR Y CONSERAR EN OBJETO
            $book = AdminBook::find(1);
            $book->delete();

        *ELIMINAR DIRECTAMENTE
            AdminBook::destroy(1); O AdminBook::destroy([1, 2, 3]);

        *ELIMINAR POR CONSULTA
            AdminBook::where('active', 0)->delete();
        */
    }

    public function findOrCreate(Request $request)
    {
        /*
         Retrieve flight by name, or create it if it doesn't exist...
            $book = AdminBook::firstOrCreate(['name' => 'Flight 10']);

        *SI NO EXISTE LO CREO CON EL ARRAY DEL SEGUNDO parametro
            $book = AdminBook::firstOrCreate(
                ['name' => 'Flight 10'],
                ['delayed' => 1, 'arrival_time' => '11:30']
            );

        *SI NO EXISTE EL REG A ACTUALIZAR LO CREA
            $book = AdminBook::updateOrCreate(
                ['name' => 'Flight 10'],
                ['delayed' => 1, 'arrival_time' => '11:30']
            );
        */
    }
}
