<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminBook extends Model
{

    use HasFactory;

    protected $guarded = ['id'];

    public function getUpdatedAtAttribute( $value ) {
        return $value? (new Carbon($value))->format('Y-m-d H:i:s') : null;
    }

    public function getCreatedAtAttribute( $value ) {
        return $value? (new Carbon($value))->format('Y-m-d H:i:s') : null;
    }
    //protected $dates = ['updated_at'];
    //protected $dateFormat = 'Y-m-d';

    /* VALORES POR DEFAULT EN CAMPOS
        protected $attributes = [
            'delayed' => false,
        ];

    *CARGAS ANSIOSAS cargar data de claves foraneas sin la necesidad de llamar al metodo de relacion
     protected $with = ['author'];  //array de models que tienen relaciones

    *VALORES PERMITIDOS PARA CREACION EN MASA


    *PERMITE TODOS LOS ATRIBUTOS CREARLOS EN MASA
        protected $guarded = [];
    */


    /*
     *METHODS SCOOPS FOR BUILDER QUERYS HELPERS
        public function scopeActive($query)
        {
            return $query->where('active', 1);
        }
        //Use or this way: $books = AdminBook::active()->otroscoop(este recibe params si quiero)->orderBy('created_at')->get();
        //Use orWhere() para unir dos scoops a modo de or sql $books = AdminBooks::popular()->orWhere->active()->get();
    //php artisan make:controller PhotoController --resource --model=Photo
     */


}
